//imports
var express = require("express");
var bodyParser = require('body-parser');
var apiRouter = require('./apiRouters').router;
//Instatiation

var app = express();
//bodyparser config : retrieve params from the body fo HTTP request
app.use(bodyParser.urlencoded(
    {
        extended: true
    }
));
app.use(bodyParser.json()); 
//Routes
app.get('/', (request, response) => {
    response.setHeader('Content-Type', 'text/html');
    response.status(200).send(
        "<h1>Bonjour Nidhal Arouri</h1>"
    );
});

app.use('/api',apiRouter);

app.listen(4010, () => {
    console.log("Server listening on 4010");
})