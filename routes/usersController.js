// Imports
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var models = require('../models');
 //Routes
module.exports = {
    register: (request, response) => {
        var email = request.body.email;
        var username = request.body.username;
        var password = request.body.password;
        var bio = request.body.bio;

        if (email == null || username == null || password == null) {
            return response.status(400).json({
                'error' : 'missing parameters'
            });
        }

        models.Users.findOne(
            {
                attributes: ['email'],
                where: {email: email}
            }
        )
        .then(userFound => {
            if( !userFound) {
                bcrypt.hash(
                    password,
                    5,
                    (err, bcryptPassword) => {
                            var newUser = models.User.create(
                                {
                                    email: email,
                                    username: username,
                                    password: bcryptPassword,
                                    bio: bio,
                                    isAdmin: 0
                                }
                            )
                            .then(
                                newUser => {
                                    return response.status(201).json({
                                        'userId': newUser.userId
                                    })
                                }
                            )
                            .catch(
                                err => {
                                    return response.status(500).json(
                                        {'error': 'cannot add User'}
                                    );
                                }
                            )
                    }
                );
            } else {
                return response.status(409).json(
                    {'error': 'user already exist'}
                );
            }
        })
        .catch( err => {
            return response.status(500).json(
                {'error' : 'unable to verify user'}
            );   
        })
    },
    login: (request, response) => {
        
    }
}